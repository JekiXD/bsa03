using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Entities;
using Newtonsoft.Json;
using BLI = CoolParking.BL.Interfaces;
using BLM = CoolParking.BL.Models;

namespace CoolParking.WebAPI.Services
{
    public class VehiclesService : IVehiclesService
    {

        private BLI.IParkingService parkingService;

        public VehiclesService(BLI.IParkingService ps)
        {
            parkingService = ps;
        }

        public bool IsIdValid(string id)
        {
            try
            {
                new BL.Models.Vehicle(id, BL.Models.VehicleType.Bus, 100);
                return true;
            }
            catch(ArgumentException)
            {
                return false;
            }
        }

        public bool IsVehicleTypeValid(int type)
        {
            return Enum.IsDefined(typeof(BLM.VehicleType), type);
        }

        public bool ExistsVehicle(string id)
        {
            try
            {
                parkingService.GetVehicles().Where(v => v.Id == id).First();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public List<Vehicle> GetVehicles()
        {
            return parkingService
                   .GetVehicles()
                   .Select(v => new Vehicle(v.Id, v.VehicleType, v.Balance))
                   .ToList();
        }

        public Vehicle GetVehicle(string id)
        {
            return parkingService
                .GetVehicles()
                .Where(v => v.Id == id)
                .Select(v => new Vehicle(v.Id, v.VehicleType, v.Balance))
                .First();
        }

        public void AddVehicle(Vehicle vehicle)
        {
            BL.Models.Vehicle v = new BL.Models.Vehicle(vehicle.Id, (BL.Models.VehicleType)vehicle.VehicleType, vehicle.Balance);
            parkingService.AddVehicle(v);
        }

        public void DeleteVehicle(string id)
        {
            parkingService.RemoveVehicle(id);
        }
    }
}