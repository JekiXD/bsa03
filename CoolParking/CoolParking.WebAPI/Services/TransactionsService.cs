using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Entities;
using Newtonsoft.Json;
using BLI = CoolParking.BL.Interfaces;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Services
{
    public class TransactionsService : ITransactionsService
    {
        private BLI.IParkingService parkingService;

        public TransactionsService(BLI.IParkingService ps)
        {
            parkingService = ps;
        }
        public bool IsIdValid(string id)
        {
            try
            {
                new BL.Models.Vehicle(id, BL.Models.VehicleType.Bus, 100);
                return true;
            }
            catch (ArgumentException)
            {
                return false;
            }
        }
        public bool ExistsVehicle(string id)
        {
            try
            {
                parkingService.GetVehicles().Where(v => v.Id == id).First();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<Transaction> GetLastTransactions()
        {
            return parkingService.GetLastParkingTransactions().Select(t => new Transaction(t.vehicleID, t.Sum, t.dateTime)).ToList();
        }

        public string GetAllTransactions()
        {
            return parkingService.ReadFromLog();
        }

        public Vehicle TopUpVehicle(TopUpVehicleTransaction t)
        {
            parkingService.TopUpVehicle(t.Id, t.Sum);
            return parkingService
                .GetVehicles()
                .Where(v => v.Id == t.Id)
                .Select(v => new Vehicle(v.Id, v.VehicleType, v.Balance))
                .First();
        }
    }
}