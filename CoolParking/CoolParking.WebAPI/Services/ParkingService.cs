using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CoolParking.WebAPI.Interfaces;
using BLI = CoolParking.BL.Interfaces;
using System.Net.Http;

namespace CoolParking.WebAPI.Services
{
    public class ParkingService : IParkingService
    {
        private BLI.IParkingService parkingService;

        public ParkingService(BLI.IParkingService ps)
        {
            parkingService = ps;
        }

        public decimal GetBalance()
        {
            return parkingService.GetBalance();
        }
        public int GetCapacity()
        {
            return parkingService.GetCapacity();
        }
        public int GetFreePlaces()
        {
            return parkingService.GetFreePlaces();
        }
    }
}
