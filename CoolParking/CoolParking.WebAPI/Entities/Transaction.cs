using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Entities
{
    public class Transaction
    {
        public Transaction(string vehicleId, decimal sum, DateTime date)
        {
            VehicleId = vehicleId;
            Sum = sum;
            TransactionDate = date;
        }
        [JsonProperty("vehicleId")]
        public string VehicleId { get; set; }

        [JsonProperty("sum")]
        public decimal Sum { get; set; }

        [JsonProperty("transactionDate")]
        public DateTime TransactionDate { get; set; }
    }
}