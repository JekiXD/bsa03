using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using BLM = CoolParking.BL.Models;

namespace CoolParking.WebAPI.Entities
{
    [JsonObject(ItemRequired = Required.Always)]
    public class Vehicle
    {
        public Vehicle(string id, BLM.VehicleType vehicleType, decimal balance)
        {
            Id = id;
            VehicleType = (int)vehicleType;
            Balance = balance;
        }
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("vehicleType")]
        public int VehicleType { get; set; }

        [JsonProperty("balance")]
        public decimal Balance { get; set; }
    }
}