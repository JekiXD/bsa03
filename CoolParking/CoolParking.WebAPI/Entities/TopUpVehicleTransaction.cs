using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Entities
{
    [JsonObject(ItemRequired = Required.Always)]
    public class TopUpVehicleTransaction
    {
        public TopUpVehicleTransaction(string vehicleId, decimal sum)
        {
            Id = vehicleId;
            Sum = sum;
        }
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("Sum")]
        public decimal Sum { get; set; }
    }
}