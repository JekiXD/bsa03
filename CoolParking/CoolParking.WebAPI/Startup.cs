using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System.IO;
using System.Reflection;
using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Services;
using BLI = CoolParking.BL.Interfaces;
using BLS = CoolParking.BL.Services;

namespace CoolParking.WebAPI
{
    public class Startup
    {
        private BLI.IParkingService parkingService;
        private BLI.ITimerService withdrawTimer;
        private BLI.ITimerService logTimer;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            withdrawTimer = new BLS.TimerService();
            logTimer = new BLS.TimerService();
            BLS.LogService logService = new BLS.LogService($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log");

            parkingService = new BLS.ParkingService(withdrawTimer, logTimer, logService);
            withdrawTimer.Start();
            logTimer.Start();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(parkingService);
            services.AddTransient<IParkingService, ParkingService>();
            services.AddTransient<IVehiclesService, VehiclesService>();
            services.AddTransient<ITransactionsService, TransactionsService>();
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "CoolParking.WebAPI", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "CoolParking.WebAPI v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
