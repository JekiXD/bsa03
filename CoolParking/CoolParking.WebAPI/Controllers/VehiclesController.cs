using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Entities;
using System.Net;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private IVehiclesService service;

        public VehiclesController(IVehiclesService vs)
        {
            service = vs;
        }

        [HttpGet]
        public ActionResult<List<Vehicle>> Get()
        {
            return Ok(service.GetVehicles());
        }


        [HttpGet("{id}")]
        public ActionResult<Vehicle> GetVehicle(string id)
        {
            if (!service.IsIdValid(id)) return BadRequest("Id is invalid");
            if (!service.ExistsVehicle(id)) return NotFound("Vehicle does not exist");
            try
            {
                return Ok(service.GetVehicle(id));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        public async Task<ActionResult<Vehicle>> AddVehicle()
        {
            try
            {
                string body = "";
                using (var stream = Request.Body)
                    using (var reader = new StreamReader(stream, Encoding.UTF8))
                         body = await reader.ReadToEndAsync();

                Vehicle vehicle = JsonConvert.DeserializeObject<Vehicle>(body);

                //Check if json value match types fo Vehicle object's properties
                dynamic staff = JObject.Parse(body);
                foreach(JProperty jproperty in staff)
                {
                    if (jproperty.Name.ToLower().Equals("vehicletype") &&
                       jproperty.Value.Type != JTokenType.Integer)
                        return BadRequest("vehicletype is not a JSON integer");
                    else
                    if (jproperty.Name.ToLower().Equals("balance") &&
                      !(jproperty.Value.Type == JTokenType.Float ||
                        jproperty.Value.Type == JTokenType.Integer))
                        return BadRequest("balance is not a JSON integer/float");
                }

                if (!service.IsVehicleTypeValid(vehicle.VehicleType)) return BadRequest("vehicletype is not valid");
                if (!service.IsIdValid(vehicle.Id)) return BadRequest("Id is invalid");

                service.AddVehicle(vehicle);
                return  StatusCode((int)HttpStatusCode.Created, body);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteVehicle(string id)
        {
            if (!service.IsIdValid(id)) return BadRequest("Id is invalid");
            if (!service.ExistsVehicle(id)) return NotFound("Vehicle does not exist");
            try
            {
                service.DeleteVehicle(id);
                return NoContent();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}