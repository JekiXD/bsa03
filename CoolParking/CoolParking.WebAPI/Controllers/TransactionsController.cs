using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Entities;
using System.Net;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private ITransactionsService service;

        public TransactionsController(ITransactionsService vs)
        {
            service = vs;
        }

        [HttpGet("last")]
        public ActionResult<List<Transaction>> GetLastTransactions()
        {
            return Ok(service.GetLastTransactions());
        }

        [HttpGet("all")]
        public ActionResult<string> GetAllTransactions()
        {
            try
            {
                return Ok(service.GetAllTransactions());
            }
            catch(InvalidOperationException)
            {
                return NotFound("Log is not found");
            }
        }

        [HttpPut("topUpVehicle")]
        public async Task<ActionResult<Vehicle>> TopUpVehicle()
        {
            try
            {
                string body = "";
                using (var stream = Request.Body)
                using (var reader = new StreamReader(stream, Encoding.UTF8))
                    body = await reader.ReadToEndAsync();

                TopUpVehicleTransaction t = JsonConvert.DeserializeObject<TopUpVehicleTransaction>(body);
                //Check if json value match types fo TopUpVehicleTransaction object's properties
                dynamic staff = JObject.Parse(body);
                foreach (JProperty jproperty in staff)
                {
                    if (jproperty.Name.ToLower().Equals("sum") &&
                      !(jproperty.Value.Type == JTokenType.Float ||
                        jproperty.Value.Type == JTokenType.Integer))
                        return BadRequest("sum is not a JSON integer/float");
                }
                
                if(!service.IsIdValid(t.Id)) return BadRequest("Id is invalid");
                if(!service.ExistsVehicle(t.Id)) return NotFound("Vehicle does not exist");
                
                return Ok(service.TopUpVehicle(t));
            }
            catch(JsonSerializationException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}