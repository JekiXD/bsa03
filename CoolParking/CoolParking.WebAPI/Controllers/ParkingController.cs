using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.WebAPI.Services;
using Microsoft.AspNetCore.Mvc;
using CoolParking.WebAPI.Interfaces;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private IParkingService service;

        public ParkingController(IParkingService ps)
        {
            service = ps;
        }
        [HttpGet]
        [Route("balance")]
        public ActionResult<decimal> GetBalance()
        {
            return Ok(service.GetBalance());
        }
        [HttpGet]
        [Route("capacity")]
        public ActionResult<int> GetCapacity()
        {
            return Ok(service.GetCapacity());
        }
        [HttpGet]
        [Route("freePlaces")]
        public ActionResult<int> GetFreePlaces()
        {
            return Ok(service.GetFreePlaces());
        }
    }
}
