using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.WebAPI.Entities;

namespace CoolParking.WebAPI.Interfaces
{
    public interface ITransactionsService
    {
        bool IsIdValid(string id);
        bool ExistsVehicle(string id);
        List<Transaction> GetLastTransactions();
        string GetAllTransactions();
        Vehicle TopUpVehicle(TopUpVehicleTransaction t);
    }
}