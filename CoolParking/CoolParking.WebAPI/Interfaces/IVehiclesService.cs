using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.WebAPI.Entities;

namespace CoolParking.WebAPI.Interfaces
{
    public interface IVehiclesService
    {
        bool IsIdValid(string id);
        bool IsVehicleTypeValid(int type);
        bool ExistsVehicle(string id);
        List<Vehicle> GetVehicles();
        Vehicle GetVehicle(string id);
        void AddVehicle(Vehicle vehicle);
        void DeleteVehicle(string id);
    }
}