﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public sealed class Parking
    {
        public decimal balance = Settings.parkingStartBalance;
        public List<Vehicle> vehicleList = new List<Vehicle>();

        private Parking() { }
        public static Parking Instance { get { return Nested.instance; } }

        private class Nested
        {
            static Nested() { }

            internal static readonly Parking instance = new Parking();
        }
    }
}