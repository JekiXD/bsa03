﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Text;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        public Parking parking = Parking.Instance;
        public List<TransactionInfo> currentTransactionList = new List<TransactionInfo>();
        private ILogService _logService;

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _logService = logService;

            withdrawTimer.Interval = Settings.paymentWithdrawPeriod;
            logTimer.Interval = Settings.logWritePeriod;

            withdrawTimer.Elapsed += WithdrawElapsedHandler;
            logTimer.Elapsed += LogElapsedHandler;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (parking.vehicleList.Count >= GetCapacity())
                throw new InvalidOperationException("Parking is full");
            else
                if (parking.vehicleList.Exists(v => v.Id == vehicle.Id))
                    throw new ArgumentException("Vehicle with such Id already exist");
                else
                    parking.vehicleList.Add(vehicle);
        }

        public void Dispose()
        {
            currentTransactionList.Clear();
            parking.vehicleList.Clear();
            parking.balance = 0;
            if (File.Exists(_logService.LogPath))
                File.Delete(_logService.LogPath);
        }

        public decimal GetBalance()
        {
            return parking.balance;
        }

        public int GetCapacity()
        {
            return Settings.parkingCapacity;
        }

        public int GetFreePlaces()
        {
            return GetCapacity() - parking.vehicleList.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return currentTransactionList.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(parking.vehicleList);
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            if (!parking.vehicleList.Exists(v => v.Id == vehicleId))
                throw new ArgumentException("Such vehicle doesn't exist");
            else
            {
                Vehicle vehicle = parking.vehicleList.Find(v => v.Id == vehicleId);

                if (vehicle.Balance < 0)
                    throw new InvalidOperationException("Vehicle is in debt");
                else
                    parking.vehicleList.Remove(vehicle);
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (!parking.vehicleList.Exists(v => v.Id == vehicleId))
                throw new ArgumentException("Such vehicle doesn't exist");
            else
                if (sum < 0)
                    throw new ArgumentException("Can't top up by negative sum");
                else
                    parking.vehicleList.Find(v => v.Id == vehicleId).Balance += sum;
        }

        private void WithdrawElapsedHandler(object sender, System.Timers.ElapsedEventArgs e)
        {
            foreach (Vehicle v in parking.vehicleList.ToArray())
            {
                decimal payment = 0.0m;

                //Calculate how much to withdraw
                if (v.Balance > 0)
                {
                    if (v.Balance >= Settings.tariffPrices[v.VehicleType.ToString()])
                    {
                        payment = Settings.tariffPrices[v.VehicleType.ToString()];
                    }
                    else
                    {
                        decimal remainder = Settings.tariffPrices[v.VehicleType.ToString()] - v.Balance;
                        payment = v.Balance + remainder * Settings.coefficientPenalty;
                    }
                }
                else
                    payment = Settings.tariffPrices[v.VehicleType.ToString()] * Settings.coefficientPenalty;

                v.Balance -= payment;
                parking.balance += payment;

                //Create transaction
                currentTransactionList.Add(new TransactionInfo { dateTime = DateTime.Now, vehicleID = v.Id, withdrawnMoney = payment });
            }
        }

        private void LogElapsedHandler(object sender, System.Timers.ElapsedEventArgs e)
        {
            StringBuilder logInfo = new StringBuilder();
            foreach (TransactionInfo ti in GetLastParkingTransactions())
            {
                logInfo.Append($"Time: {ti.dateTime}\n");
                logInfo.Append($"Vehicle Id: {ti.vehicleID}\n");
                logInfo.Append($"Withdrawn money: {ti.withdrawnMoney}\n\n");
            }

            _logService.Write(logInfo.ToString());
            currentTransactionList.Clear();
        }
    }
}