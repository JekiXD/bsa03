﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using System.Timers;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private Timer aTimer = new Timer();
        public double Interval { get { return aTimer.Interval; } set { aTimer.Interval = value; } }

        public event ElapsedEventHandler Elapsed
        {
            add { aTimer.Elapsed += value; }
            remove { aTimer.Elapsed -= value; }
        }

        public void FireElapsedEvent() {  }

        public void Start()
        {
            aTimer.Start();
        }

        public void Stop()
        {
            aTimer.Stop();
        }

        public void Dispose()
        {
            aTimer.Dispose();
        }
    }
}