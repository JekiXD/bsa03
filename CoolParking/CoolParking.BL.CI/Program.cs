﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using CoolParking.BL.Services;

namespace CoolParking.BL.CI
{
    class Program
    {
        static void Main(string[] args)
        {
            HttpClient _client = new HttpClient();
            string uri = "https://localhost:5001/";

            StringBuilder menu = new StringBuilder();
            menu.Append("*********************************************\n");
            menu.Append("| 1.Show parking balance                    |\n");
            menu.Append("| 2.Show earned money in current period     |\n");
            menu.Append("| 3.Show amount of free places on parking   |\n");
            menu.Append("| 4.Show all transactions in current period |\n");
            menu.Append("| 5.Show history of transactions            |\n");
            menu.Append("| 6.Show list of vehicles on parking        |\n");
            menu.Append("| 7.Add vehicle on parking                  |\n");
            menu.Append("| 8.Remove vehicle from parking             |\n");
            menu.Append("| 9.Top up balance of vehicle               |\n");
            menu.Append("| 0.Exit                                    |\n");
            menu.Append("*********************************************\n");

            bool loop = true;
            while (loop)
            {
                Console.WriteLine(menu);
                Console.Write("Choose here -> ");
                int choice = 0;
                try
                {
                    choice = Int32.Parse(Console.ReadLine());
                }
                catch(Exception e)
                {
                    Console.WriteLine("Invalid input.\nPress any key to continue");
                    choice = -1;
                }

                try
                {
                    Console.Clear();
                    switch (choice)
                    {
                        case 0:
                            loop = false;
                            break;
                        case 1:
                            Menu.ShowParkingBalance(_client, uri);
                            break;
                        case 2:
                            Menu.ShowEarnedMoneyInCurrentPeriod(_client, uri);
                            break;
                        case 3:
                            Menu.ShowAmountOfFreeSpotsOnParking(_client, uri);
                            break;
                        case 4:
                            Menu.ShowTransactionsInCurrentPeriod(_client, uri);
                            break;
                        case 5:
                            Menu.ShowTransactionHistory(_client, uri);
                            break;
                        case 6:
                            Menu.ShowVehiclesOnParking(_client, uri);
                            break;
                        case 7:
                            Menu.AddVehicleOnParking(_client, uri);
                            break;
                        case 8:
                            Menu.RemoveVehicleFromParking(_client, uri);
                            break;
                        case 9:
                            Menu.TopUpVehicleBalance(_client, uri);
                            break;
                        default:
                            Console.WriteLine("Such menu item doesn't exist");
                            break;

                    }
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                Console.ReadLine();
                Console.Clear();
            }
        }
    }
}
