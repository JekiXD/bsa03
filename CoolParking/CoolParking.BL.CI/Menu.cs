﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoolParking.BL.Services;
using CoolParking.BL.Models;
using System.Text.RegularExpressions;
using System.Net.Http;
using Newtonsoft.Json;
using webapiE = CoolParking.WebAPI.Entities;
using System.Net;

namespace CoolParking.BL.CI
{
    public static class Menu
    {
        public static async void ShowParkingBalance(HttpClient _client, string uri)
        {
            string balance = await _client.GetStringAsync(uri + "api/parking/balance");
            Console.WriteLine("Balance: {0}", balance);
        }

        public static async  void ShowEarnedMoneyInCurrentPeriod(HttpClient _client, string uri)
        {
            string response = await _client.GetStringAsync(uri + "api/transactions/last");

            List<webapiE.Transaction> transactions = JsonConvert.DeserializeObject<List<webapiE.Transaction>>(response);

            decimal money = 0;
            foreach (var t in transactions)
                money += t.Sum;

            Console.WriteLine("Earned money: {0}", money);
        }

        public static async void ShowAmountOfFreeSpotsOnParking(HttpClient _client, string uri)
        {
            int free = Int32.Parse(await _client.GetStringAsync(uri + "api/parking/freePlaces"));
            int notFree = Int32.Parse(await _client.GetStringAsync(uri + "api/parking/capacity")) - free;

            Console.WriteLine($"Spots Free\\Not free: {free}\\{notFree}");
        }

        public static async void ShowTransactionsInCurrentPeriod(HttpClient _client, string uri)
        {
            string content = await _client.GetStringAsync(uri + "api/transactions/last");
            List<webapiE.Transaction> transactionList = JsonConvert.DeserializeObject<List<webapiE.Transaction>>(content);


            StringBuilder sb = new StringBuilder();
            foreach (var t in transactionList)
            {
                sb.AppendLine(new string('*', 15));
                sb.AppendLine("VehicleId: " + t.VehicleId);
                sb.AppendLine("Sum: " + t.Sum);
                sb.AppendLine("TransactionDate: " + t.TransactionDate);
            }

            Console.Write(sb.ToString());
        }

        public static async void ShowTransactionHistory(HttpClient _client, string uri)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                response = await _client.GetAsync(uri + "api/transactions/all");
                response.EnsureSuccessStatusCode();
                var content = await response.Content.ReadAsStringAsync();
                Console.Write(content);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0}\n ", e.Message);
                Console.WriteLine((await response.Content.ReadAsStringAsync()));
            }
        }

        public static async void ShowVehiclesOnParking(HttpClient _client, string uri)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                response = await _client.GetAsync(uri + "api/vehicles");
                response.EnsureSuccessStatusCode();
                var content = await response.Content.ReadAsStringAsync();

                List<webapiE.Vehicle> vehicleList = JsonConvert.DeserializeObject<List<webapiE.Vehicle>>(content);
                Console.WriteLine("List of vehicle on parking");
                StringBuilder sb = new StringBuilder();
                foreach(var v in vehicleList)
                {
                    sb.AppendLine(new string('*', 15));
                    sb.AppendLine("Id: " + v.Id);
                    sb.AppendLine("VehicleType: " + (VehicleType)v.VehicleType);
                    sb.AppendLine("Balance: " + v.Balance);
                }

                Console.Write(sb.ToString());
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0}\n ", e.Message);
                Console.WriteLine((await response.Content.ReadAsStringAsync()));
            }
        }

        public static async void AddVehicleOnParking(HttpClient _client, string uri)
        {
            Regex rx = new Regex("^[A-Z]{2}-[0-9]{4}-[A-Z]{2}");
            Console.Write("Enter vehicle Id(XX-YYYY-XX)(X: A-Z, Y: 0-9): ");
            string id = Console.ReadLine();
            while(!rx.IsMatch(id))
            {
                Console.WriteLine("Invalid vehicle ID");
                Console.Write("Please, enter again: ");
                id = Console.ReadLine();
            }

            Console.Write("Enter vehicle type(0 - PassengerCar, 1 - Truck, 2 - Bus, 3 - Motorcycle): ");
            string vehicleTypeInput = Console.ReadLine();
            int vehicleType = Int32.TryParse(vehicleTypeInput, out _) ? Int32.Parse(vehicleTypeInput) : -1;
            while(vehicleType == -1 || (vehicleType < 0 || vehicleType > 3))
            {
                Console.WriteLine("Invalid vehicle type");
                Console.Write("Please, enter again: ");
                vehicleTypeInput = Console.ReadLine();
                vehicleType = Int32.TryParse(vehicleTypeInput, out _) ? Int32.Parse(vehicleTypeInput) : -1;
            }

            Console.Write("Enter vehicle Balance(>=0): ");
            string balanceInput = Console.ReadLine();
            int balance = Int32.TryParse(balanceInput, out _) ? Int32.Parse(balanceInput) : -1;
            while (balance == -1 || balance < 0)
            {
                Console.WriteLine("Invalid vehicle balance");
                Console.Write("Please, enter again: ");
                balanceInput = Console.ReadLine();
                balance = Int32.TryParse(balanceInput, out _) ? Int32.Parse(balanceInput) : -1;
            }

            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                webapiE.Vehicle vehicle = new webapiE.Vehicle(id, (VehicleType)vehicleType, balance);
                var json = JsonConvert.SerializeObject(vehicle);
                var data = new StringContent(json, Encoding.UTF8, "application/json");

                response = await _client.PostAsync(uri + "api/vehicles", data);
                response.EnsureSuccessStatusCode();

                Console.WriteLine("Vehicle is on parking");
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0}\n ", e.Message);
                Console.WriteLine((await response.Content.ReadAsStringAsync()));
            }
        }

        public static async void RemoveVehicleFromParking(HttpClient _client, string uri)
        {
            Regex rx = new Regex("^[A-Z]{2}-[0-9]{4}-[A-Z]{2}");
            Console.Write("Enter vehicle Id(XX-YYYY-XX)(X: A-Z, Y: 0-9): ");
            string id = Console.ReadLine();
            while (!rx.IsMatch(id))
            {
                Console.WriteLine("Invalid vehicle ID");
                Console.Write("Please, enter again: ");
                id = Console.ReadLine();
            }

            HttpResponseMessage response = new HttpResponseMessage();
            try
            {

                response = await _client.DeleteAsync(uri + "api/vehicles/" + id);
                response.EnsureSuccessStatusCode();

                Console.WriteLine("Vehicle has been removed");
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0}\n ", e.Message);
                Console.WriteLine((await response.Content.ReadAsStringAsync()));
            }
        }

        public static async void TopUpVehicleBalance(HttpClient _client, string uri)
        {
            Regex rx = new Regex("^[A-Z]{2}-[0-9]{4}-[A-Z]{2}");
            Console.Write("Enter vehicle Id(XX-YYYY-XX)(X: A-Z, Y: 0-9): ");
            string id = Console.ReadLine();
            while (!rx.IsMatch(id))
            {
                Console.WriteLine("Invalid vehicle ID");
                Console.Write("Please, enter again: ");
                id = Console.ReadLine();
            }


            Console.Write("Enter sum to top up(>=0): ");
            string sumInput = Console.ReadLine();
            decimal sum = Decimal.TryParse(sumInput, out _) ? Decimal.Parse(sumInput) : -1;
            while (sum == -1)
            {
                Console.WriteLine("Invalid sum");
                Console.Write("Please, enter again: ");
                sumInput = Console.ReadLine();
                sum = Decimal.TryParse(sumInput, out _) ? Decimal.Parse(sumInput) : -1;
            }

            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                webapiE.TopUpVehicleTransaction transaction = new webapiE.TopUpVehicleTransaction(id, sum);
                var json = JsonConvert.SerializeObject(transaction);
                var data = new StringContent(json, Encoding.UTF8, "application/json");

                response = await _client.PutAsync(uri + "api/transactions/topUpVehicle", data);
                response.EnsureSuccessStatusCode();

                Console.WriteLine("Balance has been toped up");
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0}\n ", e.Message);
                Console.WriteLine((await response.Content.ReadAsStringAsync()));
            }
        }
    }
}
